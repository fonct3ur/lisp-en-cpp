#pragma once

#include <string>
#include <assert.h>
#include "globals.hh"

class API;
class Cell_pair;

/*****************************/

class Cell;
using Object = Cell *;

class Cell
{
	private:
		static const uint64_t magic_number = 0xDEADBEEF;
		uint64_t magic;
		void clean();
		void check() const;


	protected:
		enum class sort { UNDEFINED, NUMBER, STRING, SYMBOL, PAIR, FUNC, CONSTANT};
		Cell();
		virtual ~Cell();
		virtual sort get_sort() const = 0;
		virtual std::string get_name_sort() const;

		static int counter_cells;


		friend class API;
		friend class Cell_pair; //to check in Cell_pair


};

/*****************************/
class Cell_number : public Cell
{
	private:
		int contents;

	private:
		Cell_number(int n);
		sort get_sort() const override;
		int get_contents() const;
		std::string get_name_sort() const override;

		friend class API;
};

/*****************************/

class Cell_string : public Cell
{
	private:
		std::string contents;

	private:
		Cell_string(std::string s);
		sort get_sort() const override;
		std::string get_contents() const;
		virtual std::string get_name_sort() const override;

		friend class API;
};

/*****************************/

class Cell_symbol : public Cell
{
	private:
		std::string contents;

	private:
		Cell_symbol(std::string s);
		sort get_sort() const override;
		std::string get_contents() const;
		std::string get_name_sort() const override;

		friend class API;
};

/*****************************/

class Cell_pair : public Cell
{
	private:
		Cell *car;
		Cell *cdr;

	private:
		Cell_pair(Cell *_car, Cell *_cdr);
		sort get_sort() const override;
		Cell *get_car() const;
		Cell *get_cdr() const;
		std::string get_name_sort() const override;

		friend class API;
};

/*****************************/
//Class to store all functions (eager subroutines and lazy functions)

class Cell_func : public Cell
{

	private:
		Cell_func(std::string the_func, bool is_lazy);
		std::string whichfunc;
		bool lazy;
		sort get_sort() const override;
		std::string get_name_sort() const override;
		std::string get_func() const;
		bool is_lazy() const;

		friend class API;
};

/*****************************/

//Class to store constants (nil, true and false)
class Cell_constant : public Cell
{

	private :

		enum class constant { NIL, TRUE, FALSE};
		Cell_constant(constant which_const);
		constant which_constant;
		sort get_sort() const override;
		std::string get_name_sort() const override;
		std::string get_name_constant() const;
		constant get_constant() const;

		friend class API;
};

/*****************************/
//functions to cast a cell pointer to a derived class pointer
Cell_number* obj_to_cell_number(Object obj);
Cell_symbol* obj_to_cell_symbol(Object obj);
Cell_string* obj_to_cell_string(Object obj);
Cell_pair* obj_to_cell_pair(Object obj);
Cell_func* obj_to_cell_func(Object obj);
Cell_constant* obj_to_cell_constant(Object obj);
