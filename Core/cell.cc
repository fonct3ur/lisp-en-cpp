#include "cell.hh"

int Cell::counter_cells = 0;


Cell::Cell()
{
	magic = magic_number;
	counter_cells++;
}

void Cell::clean()
{
	check();
	magic = 0;
}

void Cell::check() const
{
	assert(magic == magic_number);
}

Cell::~Cell()
{
	//trace("x");
	clean();
}

std::string Cell::get_name_sort() const
{
	return "UNDEFINED";
}



/*****************************/

Cell_number::Cell_number(int n) : Cell()
{
	contents = n;
}

Cell_number::sort Cell_number::get_sort() const
{
	return sort::NUMBER;
}

int Cell_number::get_contents() const
{
	return contents;
}

std::string Cell_number::get_name_sort() const
{
	return "NUMBER";
}


/*****************************/

Cell_string::Cell_string(std::string s) : Cell()
{
	contents = s;
}

Cell_string::sort Cell_string::get_sort() const
{
	return sort::STRING;
}

std::string Cell_string::get_contents() const
{
	return contents;
}

std::string Cell_string::get_name_sort() const
{
	return "STRING";
}


/*****************************/

Cell_symbol::Cell_symbol(std::string s) : Cell()
{
	contents = s;
}

Cell_symbol::sort Cell_symbol::get_sort() const
{
	return sort::SYMBOL;
}

std::string Cell_symbol::get_contents() const
{
	return contents;
}

std::string Cell_symbol::get_name_sort() const
{
	return "SYMBOL";
}

/*****************************/

Cell_pair::Cell_pair(Cell* _car, Cell* _cdr) : Cell()
{
	this->check();

	_car->check();
	_cdr->check();
	car = _car;
	cdr = _cdr;
}

Cell_pair::sort Cell_pair::get_sort() const
{
	return sort::PAIR;
}

Cell* Cell_pair::get_car() const
{
	return car;
}

Cell* Cell_pair::get_cdr() const
{
	return cdr;
}

std::string Cell_pair::get_name_sort() const
{
	return "PAIR";
}

/*****************************/


Cell_func::Cell_func(std::string the_func, bool is_lazy) : Cell()
{
	whichfunc = the_func;
	lazy = is_lazy;
}

Cell_func::sort Cell_func::get_sort() const
{
	return sort::FUNC;
}

std::string Cell_func::get_func() const
{
	return whichfunc;
}

std::string Cell_func::get_name_sort() const
{
	return "<FUNC>";
}

bool Cell_func::is_lazy() const
{
	return lazy;
}

/*****************************/

Cell_constant::Cell_constant(constant _which_constant) : Cell()
{
	which_constant = _which_constant;
}

Cell_constant::sort Cell_constant::get_sort() const
{
	return sort::CONSTANT;
}

Cell_constant::constant Cell_constant::get_constant() const
{
	return which_constant;
}

std::string Cell_constant::get_name_sort() const
{
	return "<CONSTANT>";
}

std::string Cell_constant::get_name_constant() const
{
	if (which_constant == constant::FALSE)
		return string_false;
	else if (which_constant == constant::TRUE)
		return string_true;
	else
		return string_nil;
}
/*****************************/

Cell_number* obj_to_cell_number(Object obj)
{
	Cell_number* objt = dynamic_cast<Cell_number*>(obj);
	assert(objt != nullptr);
	return objt;
}

Cell_symbol* obj_to_cell_symbol(Object obj)
{
	Cell_symbol* objt = dynamic_cast<Cell_symbol*>(obj);
	assert(objt != nullptr);
	return objt;
}

Cell_string* obj_to_cell_string(Object obj)
{
	Cell_string* objt = dynamic_cast<Cell_string*>(obj);
	assert(objt != nullptr);
	return objt;
}

Cell_pair* obj_to_cell_pair(Object obj)
{
	Cell_pair* objt =  dynamic_cast<Cell_pair*>(obj);
	assert(objt != nullptr);
	return objt;
}

Cell_func* obj_to_cell_func(Object obj)
{
	Cell_func* objt =  dynamic_cast<Cell_func*>(obj);
	assert(objt != nullptr);
	return objt;
}

Cell_constant* obj_to_cell_constant(Object obj)
{
	Cell_constant* objt =  dynamic_cast<Cell_constant*>(obj);
	assert(objt != nullptr);
	return objt;
}
