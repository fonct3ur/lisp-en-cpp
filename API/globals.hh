#pragma once

class Cell;

using Object = Cell *;
using Env = Object;

//file to set predefined names.

//constants
const std::string string_true = "#t";
const std::string string_false = "#f";
const std::string string_nil = "()";

//eager functions
const std::string string_plus = "+";
const std::string string_minus = "-";
const std::string string_times = "*";
const std::string string_eq = "=";
const std::string string_div = "/";
const std::string string_mod = "mod";
const std::string string_less = "<";
const std::string string_greater = ">";
const std::string string_lambda = "lambda";
const std::string string_cons = "cons";
const std::string string_display = "display";

//lazy functions
const std::string string_quote = "quote";
const std::string string_if = "if";
const std::string string_print_env = "printenv";
const std::string string_begin = "begin";
const std::string string_cond = "cond";
const std::string string_let = "let";

//define
const std::string string_define = "define";


//some colors to print
const std::string bold = "\e[1m";
const std::string non_bold = "\e[0m";
const std::string green_color = "\033[32m";
const std::string red_color = "\033[31m";
const std::string default_color = "\033[39m";
