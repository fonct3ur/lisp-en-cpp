#pragma once

#include <iostream>
#include <string>
#include "globals.hh"
#include "cell.hh"
#include <string>

#include <unordered_map>


class API
{
	// Object

	public:
		static void check(Object l);

		static Object nil();
		static bool null(Object l);
		static Object t();
		static Object f();

		//list processing
		static Object cons(Object a, Object l);
		static Object car(Object l);
		static Object cdr(Object l);

		static int get_size_list(Object l);

		static bool eq(Object a, Object b);

		//test predicats
		static bool numberp(Object l);
		static bool stringp(Object l);
		static bool symbolp(Object l);
		static bool listp(Object l);
		static bool funcp(Object l);
		static bool constantp(Object l);

		//allow to identify functions
		static bool lazy_funcp(Object l);
		static bool eager_funcp(Object l);
		static bool identify_func(std::string s, Object l);

		//convert a c++ thing to a lisp thing
		static Object number_to_object(int n);
		static Object string_to_object(std::string s);
		static Object symbol_to_object(std::string s);
		static Object bool_to_object(bool b);

		//convert lisp things to c++ things
		static int object_to_number(Object l);
		static std::string object_to_string(Object l);
		static bool object_to_bool(Object l);

		//used on apply_lambda
		static Object get_func_lambda();

		//get a stream from ojects
		static std::ostream& object_to_stream(std::ostream& s, Object l);

		static int get_nb_objects_initialized();

	private:

		//map to store all functions and constants
		static const std::unordered_map<std::string, Object> objects_map;

		// Environment
	public:
		static Env make_env();
		static Env add_new_binding(std::string name, Object value, Env env);
		static Object find_value(std::string name, Env env);
		static std::ostream& print_env(std::ostream& s, Env env);
		static bool in_envp(std::string name, Env env);
};
