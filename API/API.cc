#include "API.hh"
#include <assert.h>

using namespace std;




//constants and functions
const std::unordered_map<std::string, Object> API::objects_map
({
 //constants
 { string_nil, new Cell_constant(Cell_constant::constant::NIL) },
 { string_true, new Cell_constant(Cell_constant::constant::TRUE) },
 { string_false, new Cell_constant(Cell_constant::constant::FALSE) },

 //eager
 { string_plus, new Cell_func(string_plus, false) },
 { string_cons, new Cell_func(string_cons, false) },
 { string_minus, new Cell_func(string_minus, false) },
 { string_times, new Cell_func(string_times, false) },
 { string_div, new Cell_func(string_div, false) },
 { string_eq, new Cell_func(string_eq, false) },
 { string_less, new Cell_func(string_less, false) },
 { string_greater, new Cell_func(string_greater, false) },
 { string_mod, new Cell_func(string_mod, false) },
 { string_display, new Cell_func(string_display, false) },

 //lazy
 { string_print_env, new Cell_func(string_print_env, true) },
 { string_lambda, new Cell_func(string_lambda, true) },
 { string_quote, new Cell_func(string_quote, true) },
 { string_begin, new Cell_func(string_begin, true) },
 { string_cond, new Cell_func(string_cond, true) },
 { string_let, new Cell_func(string_let, true) },
 { string_if, new Cell_func(string_if, true) }
});

/*
Check function, useful to make sure we do not manipulate wrong objects
*/
void API::check(Object l)
{
	assert(l != nullptr);
	l->check();
}

/*
getters for constants
*/
Object API::nil() { return objects_map.at(string_nil); }
Object API::t() { return objects_map.at(string_true); }
Object API::f() { return objects_map.at(string_false); }

/*
getter for lambda function
*/
Object API::get_func_lambda() {	return objects_map.at(string_lambda); }


/*
list manipulating functions
*/
bool API::null(Object l)
{
	API::check(l);
	return l == nil();
}

Object API::cons(Object a, Object l)
{
	API::check(a);
	API::check(l);
	if (listp(l))
	{
		Object obj = new Cell_pair(a, l);
		return obj;
	}
	else
	{
		Object obj = new Cell_pair(l, nil());
		Object obj2 = new Cell_pair(a, obj);
		return obj2;
	}
}

Object API::car(Object l)
{
	API::check(l);
	if(not listp(l) && (not null(l))) throw string("unreadable error.");
	Object obj = obj_to_cell_pair(l)->get_car();
	return obj;
}

Object API::cdr(Object l)
{
	API::check(l);
	if(not listp(l) && (not null(l))) throw string("unreadable error.");
	Object obj = obj_to_cell_pair(l)->get_cdr();
	return obj;
}

int API::get_size_list(Object l)
{
	API::check(l);
	assert(listp(l));
	if(null(l))
		return 0;
	else
		return (1 + get_size_list(cdr(l)));
}


/*
function to test equality between Objects
*/
bool API::eq(Object a, Object b)
{
	API::check(a);
	API::check(b);

	if(numberp(a) && numberp(b))
		return (object_to_number(a) == object_to_number(b));
	else if(stringp(a) && stringp(b))
		return (object_to_string(a) == object_to_string(b));
	else if(symbolp(a) && symbolp(b))
		return (object_to_string(a) == object_to_string(b));
	else
		return a == b;
}

/*
Predicats to understand the nature of the Object we are manipulating
*/
bool API::numberp(Object l)
{
	API::check(l);
	return l->get_sort() == Cell::sort::NUMBER;
}

bool API::stringp(Object l)
{
	API::check(l);
	return l->get_sort() == Cell::sort::STRING;
}

bool API::symbolp(Object l)
{
	API::check(l);
	return l->get_sort() == Cell::sort::SYMBOL;
}

bool API::listp(Object l)
{
	API::check(l);
	return ((l->get_sort() == Cell::sort::PAIR) || null(l));
}

bool API::funcp(Object l)
{
	API::check(l);
	return ((l->get_sort() == Cell::sort::FUNC));
}

bool API::constantp(Object l)
{
	API::check(l);
	return ((l->get_sort() == Cell::sort::CONSTANT));
}


/*
functions to caracterize lisp subroutines and control structures
*/

bool API::lazy_funcp(Object l)
{
	API::check(l);
	if(not funcp(l)) return false;
	return obj_to_cell_func(l)-> is_lazy();
}

bool API::eager_funcp(Object l)
{
	API::check(l);
	if(not funcp(l)) return false;
	return not (obj_to_cell_func(l)-> is_lazy());
}

bool API::identify_func(string s, Object l)
{
	API::check(l);
	if(not funcp(l)) return false;
	return	s == (object_to_string(l));
}


/*
functions to map C++ objects to Lisp objects
*/
Object API::number_to_object(int n)
{
	Object obj = new Cell_number(n);
	return obj;
}

Object API::string_to_object(std::string s)
{
	Object obj = new Cell_string(s);
	return obj;
}

Object API::symbol_to_object(std::string s)
{
	Object obj = new Cell_symbol(s);
	return obj;
}

Object API::bool_to_object(bool b)
{
	if (b)
		return t();
	return f();

}


/*
functions to map Lisp objects to C++ objects.
object_to_string is also used for printing objects.
*/
int API::object_to_number(Object l)
{
	API::check(l);
	if(not numberp(l)) throw string ("unreadable error.");
	return obj_to_cell_number(l)->get_contents();
}

bool API::object_to_bool(Object l)
{
	API::check(l);
	if (eq(l, f()) || null(l))
	{
		return false;
	}
	//everything that is not false is true
	return true;
}

std::string list_to_string(Object l)
{
	API::check(l); assert(API::listp(l));
	if (API::null(l))
		return "";

	string first_object =  API::object_to_string(API::car(l));

	if (API::null(API::cdr(l)))
		return first_object;
	else
		return first_object + " " + list_to_string(API::cdr(l));
}

string API::object_to_string(Object l)
{
	API::check(l);
	if (stringp(l))
		return obj_to_cell_string(l)->get_contents();
	else if(numberp(l))
		return std::to_string(obj_to_cell_number(l)->get_contents());
	else if (symbolp(l))
		return obj_to_cell_symbol(l)->get_contents();
	else if(funcp(l))
		return obj_to_cell_func(l)->get_func();
	else if(constantp(l))
		return obj_to_cell_constant(l)->get_name_constant();
	else
	{
		assert(listp(l));
		return "(" + list_to_string(l) +  ")";
	}
}


/*
code to print objects
*/
std::ostream& API::object_to_stream(std::ostream& s, Object l)
{
	return s << API::object_to_string(l);
}

/*
function to know how many objects have been initialized since the beggining
*/
int API::get_nb_objects_initialized()
{
	return Cell::counter_cells;
}



/*  ENV    */
Env API::make_env()
{
	Env env =  API::nil();

	for (std::pair<std::string, Object> element : objects_map)
	{
		env = add_new_binding(element.first, element.second, env);
	}

	//to stop printing inventory before subroutines
	env = add_new_binding("###", string_to_object("###"), env);

	return env;
}

Env API::add_new_binding(string name, Object value, Env env)
{
	API::check(env);
	Object obj = new Cell_pair(string_to_object(name), value);
	return cons(obj, env);
}

Object API::find_value(string name, Env env)
{
	API::check(env);
	while (not API::null(env))
	{
		Object pair = car(env);
		if (object_to_string(car(pair)) == name)
			return cdr(pair);
		else
			return API::find_value(name,cdr(env));
	};
	assert(false);
}

ostream& print_env_aux(ostream& s, Env env)
{
	API::check(env);
	assert(API::listp(env));

	if (API::null(env))
		return s;

	//get actual link
	Object obj = API::car(env);
	assert(API::listp(obj));

	//if the object if this redifined symbol, stop printing environment
	//(it's the separation between defines made by user and subroutines defines)
	if (API::object_to_string(API::car(obj)) == "###") return s;

	//print the object name and value
	s << "(" << API::object_to_string(API::car(obj)) << " "
		<< API::object_to_string(API::cdr(obj)) << ") ; ";

	//print the rest of the environment
	return print_env_aux(s, API::cdr(env));
}

ostream& API::print_env(ostream& s, Env env)
{
	API::check(env);
	assert(listp(env));
	s << "Environment : ";
	return print_env_aux(s, env);
}

bool API::in_envp(string name, Env env)
{
	API::check(env);
	while (not API::null(env))
	{
		Object pair = car(env);
		if (object_to_string(car(pair)) == name)
			return true;
		else
			return API::in_envp(name,cdr(env));
	};
	return false;
}
