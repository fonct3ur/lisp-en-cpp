#include "defs.hh"

#include <string>
#include <iostream>
#include <assert.h>

#include "library.hh"
#include "eval.hh"

/* do_lambda replaces the symbol lambda by the corresponding <FUNC>
   without touching the rest of the list */
Object do_lambda(Object l)
{
	check(l);
	assert(listp(l));
	return cons(get_func_lambda() ,l);
}

/* if it gets exactly one argument, quote returns it without any evaluation*/
Object do_quote(Object l)
{
	check(l);
	assert(listp(l));

	if(get_size_list(l) == 0) throw string("quote without argument.");
	if(get_size_list(l) > 1) throw string("quote with too many arguments.");

	return car(l);
}

/* add the first element of the list l to its second element if it's a list
   or create a list with the two elements if it isn't*/
Object do_cons(Object l)
{
	check(l);
	assert(listp(l));

	if(get_size_list(l) == 0) throw string("cons without argument.");
	if(get_size_list(l) == 1) throw string("cons with one argument.");
	if(get_size_list(l) > 2) throw string("cons with too many arguments.");

	return cons(car(l), cadr(l));

}

/* does the if instruction : the first argument is evaluated as a boolean.
   If it is true, we evaluate and return the second argument. Otherwise, the third.
   The third argument is optionnal.
   If it is not present, nil is returned when the first is false */
Object do_if(Object l, Env env)
{
	check(l);
	check(env);

	try
	{
		if(get_size_list(l) == 0) throw string("if without arguments.");
		if(get_size_list(l) == 1) throw string("if with one argument.");
		if(get_size_list(l) >  3) throw string ("if with too many arguments.");

		Object test_part = car(l);
		if (object_to_bool(eval(test_part, env)))
		{
			Object then_part = cadr(l);
			return eval(then_part,env);
		}
		else if (not null(cddr(l)))
		{
			Object else_part = caddr(l);
			return eval(else_part, env);
		}
		else
		{
			return nil();
		}
	}
	catch(string e)
	{
		throw string("if error : " + e);
	}
}

/* does the + operation with n arguments. If there are none 0 is returned.*/
Object do_plus(Object lvals)
{
	try
	{
		check(lvals);
		assert(listp(lvals));
		if (null(lvals)) return number_to_object(0);
		else if (null(cdr(lvals)))
		{
			Object rval = car(lvals);
			string descr = object_to_string(rval);
			if(not numberp(rval))
			{
				throw string ("not a number : " + descr + ".");
			}
			return car(lvals);
		}
		else
		{
			int a = object_to_number(car(lvals));
			int b = object_to_number(cadr(lvals));
			Object tot = number_to_object(a+b);
			if (not null(cddr(lvals)))
			{
				return do_plus(cons(tot, cddr(lvals)));
			}
			return tot;
		}
	}
	catch(string e)
	{
		throw string ("error in a + func : " + e);
	}
}

/* does the - operation with n arguments :
   substracts the n-1 last arguments to the first one.
   If there are none 0 is returned.*/
Object do_minus(Object lvals)
{
	try
	{
		check(lvals);
		assert(listp(lvals));
		if (null(lvals)) return number_to_object(0);
		else if (null(cdr(lvals)))
		{
			Object rval = car(lvals);
			string descr = object_to_string(rval);
			if(not numberp(rval))
			{
				throw string ("not a number : " + descr + ".");
			}
			return car(lvals);
		}
		else
		{
			int a = object_to_number(car(lvals));
			int b = object_to_number(cadr(lvals));
			Object tot = number_to_object(a-b);
			if (not null(cddr(lvals)))
			{
				return do_minus(cons(tot, cddr(lvals)));
			}
			return tot;
		}
	}
	catch(string e)
	{
		throw string ("error in a - func : " + e);
	}
}

/* do the * operation with n arguments. If there are none 1 is returned.*/
Object do_times(Object lvals)
{
	try
	{
		check(lvals);
		assert(listp(lvals));
		if (get_size_list(lvals) == 0) return number_to_object(1);
		else if (get_size_list(lvals) == 1)
		{
			Object rval = car(lvals);
			string descr = object_to_string(rval);
			if(not numberp(rval))
			{
				throw string ("not a number : " + descr + ".");
			}
			return car(lvals);
		}
		else
		{
			int a = object_to_number(car(lvals));
			int b = object_to_number(cadr(lvals));
			Object tot = number_to_object(a*b);
			if (not null(cddr(lvals)))
			{
				return do_times(cons(tot, cddr(lvals)));
			}
			return tot;
		}
	}
	catch(string e)
	{
		throw string ("error in a * func : " + e);
	}

}

/* do the = operation with n arguments. If there is none true is returned.*/
Object do_eq(Object lvals)
{
	check(lvals);
	assert(listp(lvals));
	if (get_size_list(lvals) <= 1)
		return t();
	else
	{
		if (eq(car(lvals), cadr(lvals)))
		{
			if (get_size_list(lvals) > 2)
				return do_eq(cdr(lvals));
			else
				return bool_to_object(true);
		}
		else
			return bool_to_object(false);
	}
}

/* do the Euclidean division with n arguments in the given order.
   If there are none 1 is returned. */
Object do_div(Object lvals)
{
	try
	{
		check(lvals);
		assert(listp(lvals));
		if (get_size_list(lvals) == 0)
			return number_to_object(1);
		else if (get_size_list(lvals) == 1)
		{
			Object rval = car(lvals);
			string descr = object_to_string(rval);
			if(not numberp(rval))
				throw string ("not a number : " + descr + ".");
			return car(lvals);
		}
		else
		{
			int a = object_to_number(car(lvals));
			int b = object_to_number(cadr(lvals));
			Object tot = number_to_object(a/b);
			if (get_size_list(lvals) > 2)
				return do_div(cons(tot, cddr(lvals)));
			return tot;
		}
	}
	catch(string e)
	{
		throw string ("error in a / func : " + e);
	}

}

/* do the C++ % operation with n arguments in the given order.
   If there are none 0 is returned. */
Object do_mod(Object lvals)
{
	try
	{
		check(lvals);
		assert(listp(lvals));
		if (get_size_list(lvals) == 0)
			return number_to_object(0);
		else if (get_size_list(lvals) == 1)
		{
			Object rval = car(lvals);
			string descr = object_to_string(rval);
			if(not numberp(rval))
				throw string ("not a number : " + descr + ";");
			return car(lvals);
		}
		else
		{
			int a = object_to_number(car(lvals));
			int b = object_to_number(cadr(lvals));
			Object tot = number_to_object(a%b);
			if (get_size_list(lvals) > 2)
			{
				return do_mod(cons(tot, cddr(lvals)));
			}
			return tot;
		}
	}
	catch(string e)
	{
		throw string ("error in a mod func : " + e);
	}

}

/* do the chained < operation with n arguments :
   (< a b c) returns true if and only if a < b and b < c.
   If there are no arguments true is returned. */
Object do_less(Object lvals)
{
	check(lvals);
	assert(listp(lvals));
	if (get_size_list(lvals) <= 1) return t();
	else
	{
		int a = object_to_number(car(lvals));
		int b = object_to_number(cadr(lvals));
		if (a<b)
		{
			if (get_size_list(lvals) > 2)
				return do_less(cdr(lvals));
			else
				return bool_to_object(true);
		}
		return bool_to_object(false);
	}
}

/* do the chained > operation with n arguments :
   (> a b c) returns true if and only if a > b and b > c.
   If there are no arguments true is returned. */
Object do_greater(Object lvals)
{
	check(lvals);
	assert(listp(lvals));
	if (get_size_list(lvals) <= 1) return t();
	else
	{
		int a = object_to_number(car(lvals));
		int b = object_to_number(cadr(lvals));
		if (a>b)
		{
			if (get_size_list(lvals) > 2)
				return do_greater(cdr(lvals));
			else
				return bool_to_object(true);
		}
		return bool_to_object(false);
	}
}

/*print the environment given in argument*/
Object do_print_env(Env env)
{
	check(env);
	print_env(std::cout, env);
	return nil();
}

Object do_display(Object l)
{
	check(l);
	assert(listp(l));

	cout << car(l);
	return car(l);
}

/*do the begin instruction which consists of evaluating the expressions in the
  given order*/
Object do_begin(Object l, Env env)
{
	check(l);
	check(env);
	assert(listp(l));
	if (get_size_list(l) == 0)
		return nil();
	while(get_size_list(l) >= 1)
	{
		eval(car(l), env);
		l = cdr(l);
	}
	return eval(car(l), env);
}


/*do the cond instruction. Each given argument consist of a boolean and an
  expression, only the first one with true as a boolean is evaluated*/
Object do_cond(Object largs, Env env)
{
	check(largs);
	check(env);
	assert(listp(largs));
	while(not null(largs))
	{
		Object pair = car(largs);
		Object boolean_in_pair = eval(car(pair), env);
		if (object_to_bool(boolean_in_pair))
			return eval(cadr(pair), env);
		largs = cdr(largs);
	}
	return nil();
}

/*do the let instruction which add locally a sequence of binding to the
  environment before evaluating a given expression*/
Object do_let(Object l, Env env)
{
	check(l);
	assert(listp(l));

	Object new_bindings = car(l);
	Object let_body = cadr(l);

	check(new_bindings);
	check(let_body);
	assert(listp(new_bindings));

	try
	{
		Env new_env = env;

		while(not null(new_bindings))
		{
			Object new_bind = car(new_bindings);
			Object new_func = car(new_bind);
			Object new_value = eval(cadr(new_bind), env);
			new_env = add_new_binding(object_to_string(new_func),
					new_value, new_env);
			new_bindings = cdr(new_bindings);
		}
		return eval(let_body, new_env);
	}
	catch(string e)
	{
		throw string ("error in a let : " + e);
	}
}
/*
Object do_greater(Object lvals){
	int a = object_to_number(car(lvals));
	int b = object_to_number(cadr(lvals));
	return bool_to_object(a>b);
}

*/
