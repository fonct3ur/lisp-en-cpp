#include "toplevel.hh"

#include <string>
#include <iostream>
#include <sstream>
#include <assert.h>

#include "read.hh"
#include "eval.hh"
#include "defs.hh"
#include "globals.hh"

using namespace std;

Toplevel::Toplevel()
{
	global_env = make_env();
}

void Toplevel::go ()
{
	int count = 1;
	Object env = this->global_env;
	try
	{
		while(true)
		{
			try
			{
				//the prompt
				cout << green_color << bold << count << " > "
					<< default_color << non_bold << flush;
				count++;

				Object l = read_object();
				cout << bold << "=> " << non_bold;
				Object ll = eval_toplevel(l, &env);
				cout << ll << endl << endl;
			}
			catch(string error)
			{
				int str_len = error.length();
				if(str_len > 300)
				{
					cout << "An error is about to be generated. Because the "
						<< "error message was too long, part of it will"
						<< "be omited."
						<< endl;
					string err_deb = error.substr(0,100);
					string err_end = error.substr(str_len - 100, 100);

					error = err_deb + " [...] " + err_end;
				}
				cout << bold << red_color << "Lisp error : " << non_bold << default_color << error  << endl;
			}
		}
	}
	catch(runtime_error& e)
	{
		cout << endl;
		clog << "End of session, you used " << get_nb_objects_initialized()
			<< " objects." << endl;
		throw runtime_error ("");
	}
}


// The evaluation function called by the toplevel. The only one handle define
Object eval_toplevel(Object l, Env* ptrenv)
{
	if(definep(l)){
		return do_define(l, ptrenv);
	}
	return eval(l, *ptrenv);
}

// Checks wether l has the form of a define instruction
bool definep(Object l)
{
	if(not listp(l)) return false;
	if(null(l)) return false;
	if(symbolp(car(l)))
	{
		if(object_to_string(car(l)) == string_define) return true;
	}
	return false;
}

Object do_define(Object l, Env* ptrenv)
{
	try
	{
		Object to_eval = caddr(l);
		Object symbol = cadr(l);
		string new_def;

		/* macro generation of lambda expressions*/
		if (listp(symbol))
		{
			if (null(symbol))
			{
				throw string ("syntax error.");
			}
			to_eval = cons(symbol_to_object("lambda"),
					cons(cdr(symbol), cons(to_eval, nil())));
			new_def = object_to_string(car(symbol));
		}
		/* normal case (no macro generation) */
		else
		{
			new_def = object_to_string(symbol);
		}

		// The rest of the function is the same in both cases
		Object value = eval(to_eval, *ptrenv);
		*ptrenv = add_new_binding(new_def, value, *ptrenv);

		string def_message;
		def_message =  "New definition : (" + new_def + " -> " +
			object_to_string(value) + ")";
		return string_to_object(def_message);
	}
	catch (string e)
	{
		throw string ("error in define : " + e );
	}
}
