#include "apply.hh"

#include <iostream>
#include <stdexcept>
#include <assert.h>
#include <string>
#include "globals.hh"
#include "library.hh"
#include "eval.hh"
#include "defs.hh"

using namespace std;


Object apply_lazy(Object fun, Object largs, Env env)
{
	if(identify_func(string_print_env, fun))
		return do_print_env(env);
	if(identify_func(string_quote, fun))
		return do_quote(largs);
	if(identify_func(string_if, fun))
		return do_if(largs, env);
	if(identify_func(string_begin, fun))
		return do_begin(largs, env);
	if(identify_func(string_cond, fun))
		return do_cond(largs, env);
	if(identify_func(string_let, fun))
		return do_let(largs, env);
	if(identify_func(string_lambda, fun))
		return do_lambda(largs);
	assert(false);
}

Object apply_eager(Object fun, Object lvals)
{
	if(identify_func(string_cons, fun))
		return do_cons(lvals);
	if(identify_func(string_plus, fun))
		return do_plus(lvals);
	if(identify_func(string_times, fun))
		return do_times(lvals);
	if(identify_func(string_div, fun))
		return do_div(lvals);
	if(identify_func(string_minus, fun))
		return do_minus(lvals);
	if(identify_func(string_eq, fun))
		return do_eq(lvals);
	if(identify_func(string_less, fun))
		return do_less(lvals);
	if(identify_func(string_greater, fun))
		return do_greater(lvals);
	if(identify_func(string_mod, fun))
		return do_mod(lvals);
	if(identify_func(string_display, fun))
		return do_display(lvals);
	assert(false);
}
