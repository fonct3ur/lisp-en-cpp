#pragma once

#include <iostream>
#include <stdexcept>
#include <assert.h>
#include <string>
#include "globals.hh"
#include "library.hh"
#include "eval.hh"
#include "defs.hh"


//lazy function, need environment
Object apply_lazy(Object fun, Object largs, Object env);

//eager functions, no need for environment
Object apply_eager(Object fun, Object lvals);
