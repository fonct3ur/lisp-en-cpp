#pragma once

#include "library.hh"

using namespace std;

class Toplevel
{

	private:
		Env global_env;

	public:
		Toplevel();
		void go();
};

bool definep(Object l);
Object do_define(Object l, Env* ptrenv);
Object eval_toplevel(Object l, Env* ptrenv);
