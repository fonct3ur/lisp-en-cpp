#include "eval.hh"

#include <iostream>
#include <stdexcept>
#include <assert.h>
#include <string>
#include <assert.h>

#include "globals.hh"
#include "library.hh"
#include "defs.hh"
#include "apply.hh"
#include "lambda.hh"

using namespace std;


/* The main eval function.
   This is the evaluation function called by other functions.
   The implementation of a trace function would have to be done here.*/
Object eval(Object l, Env env)
{
	try
	{
		return eval_core(l, env);
	}
	catch(string e)
	{
		throw string ("evaluation error : " + e);
	}
}

/* Checks for different cases and calls the appropriate auxillary functions */
Object eval_core(Object l, Env env)
{
	check(l);
	check(env);

	//if l is a non-empty list, uses the eval_fun made for functions
	if(listp(l))
	{
		if(null(l))
			return nil();
		else
			return eval_fun(car(l), cdr(l), env);
	}
	/* if l is a symbol we look in the environment
	   (and throw an error if l isn't in it) */
	if(symbolp(l))
	{
		try
		{
			// searches the environment for the given symbol
			if(in_envp(object_to_string(l), env))
				return find_value(object_to_string(l), env);
			throw std::string ("unknown symbol : " + object_to_string(l) + ".");
		}
		catch(string e)
		{
			throw string ("bad symbol : " + e);
		}

	}
	/* The evaluation of anything that isn't a symbol or a list
	   simply return the thing in question */
	return l;
}

/* Eval_fun handles the evaluation of non-empty lists.
   obj1 is the object in functionnal position whereas obj2 is the list
   of the others objects in the list we wish to evaluate (the arguments).
   Eval_fun simply discriminates between three possibilities.*/
Object eval_fun(Object obj1, Object obj2, Env env)
{
	Object eva_fun = eval(obj1, env);

	/* if the function is lazy it calls apply_lazy, passing parameters by name
	   and the environment */
	if(lazy_funcp(eva_fun))
	{
		try
		{
			return apply_lazy(eva_fun, obj2, env);
		}
		catch(string e)
		{
			throw string ("application error : " + e);
		}
	}
	/* If the function is eager we evaluate all of it's arguments and apply the
	   function, passing arguments by value and without passing the environment. */
	if(eager_funcp(eva_fun))
	{
		try
		{
			return apply_eager(eva_fun, eval_list(obj2, env));

		}
		catch(string e)
		{
			throw string ("application error : " + e);
		}
	}
	/* Ff the function is a lambda expression we evaluates all of it's arguments
	   and apply the function, passing arguments by value and also
	   passing the environment. */
	if(lambdalistp(eva_fun))
	{
		try
		{
			return apply_lambda(eva_fun, eval_list(obj2, env), env);

		}
		catch(string e)
		{
			throw string ("lambda error : " + e);
		}
	}
	// Error if no of the above cases are matched.
	throw string ("unlawfull object in functionnal position : "
			+ object_to_string(eva_fun) + ".");
}

// A simple auxillary function. Evaluates all the elements in a List.
Object eval_list(Object obj, Env env)
{
	assert(listp(obj));
	if(null(obj)) return obj;

	Object evaluated = eval(car(obj), env);
	Object next = cdr(obj);
	return cons(evaluated, eval_list(next, env));
}
