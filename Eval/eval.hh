#pragma once

#include <stdexcept>
#include <iostream>
#include <stdexcept>
#include <string>

#include "globals.hh"

using namespace std;

Object eval(Object l, Env env);
Object eval_core(Object l, Env env);
Object eval_list(Object obj, Object Env);
Object eval_fun(Object obj1, Object obj2, Env env);
bool lambdalistp(Object obj);
