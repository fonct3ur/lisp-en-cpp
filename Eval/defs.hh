#pragma once

#include <string>
#include "library.hh"
#include "eval.hh"

//EAGER functions
Object do_lambda(Object l);
Object do_plus(Object lvals);
Object do_minus(Object lvals);
Object do_times(Object lvals);
Object do_eq(Object lvals);
Object do_div(Object lvals);
Object do_mod(Object lvals);
Object do_less(Object lvals);
Object do_greater(Object lvals);
Object do_cons(Object lvals);

//LAZY functions
Object do_quote(Object l);
Object do_if(Object l, Env env);
Object do_begin(Object l, Env env);
Object do_display(Object l);
Object do_cond(Object largs, Env env);
Object do_print_env(Env env);
Object do_let(Object l, Env env);
