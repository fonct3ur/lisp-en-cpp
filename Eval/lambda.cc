#include "lambda.hh"

#include <iostream>
#include <stdexcept>
#include <assert.h>
#include <string>

#include "globals.hh"
#include "library.hh"
#include "defs.hh"
#include "eval.hh"

using namespace std;

/* This function tests wether obj is a list starting with lambda and containing
   at least two elements */
bool lambdalistp(Object obj)
{
	if(not listp(obj)) return false;
	if(null(obj)) return false;
	if(null(cdr(obj))) return false;
	if(identify_func(string_lambda, car(obj))) return true;
	return false;
}

/* To apply a lambda, we use the values passed as arguments to create a
   temporary environment in which we will evaluate the body */
Object apply_lambda(Object fun, Object args, Env env)
{
	assert(lambdalistp(fun));

	Object vars = cadr(fun);
	Object body = caddr(fun);
	Env new_env = add_to_env(vars, args, env);
	return eval(body, new_env);
}

/* we create a temporary environment on top of the one we were given */
Env add_to_env(Object vars, Object args, Env env)
{
	assert(listp(vars));
	assert(listp(args));

	if (null(vars) && null(args))
	{
		return env;
	}
	else if (listp(vars) && listp(args) && (not null(vars)) && (not null(args)))
		//note that vars and args both are non empty sequences
	{
		string name = object_to_string(car(vars));
		Env new_env = add_new_binding(name, car(args), env);
		return add_to_env(cdr(vars), cdr(args), new_env);
	}
	else
	{
		throw std::string("incorrect number of arguments");
	}
}
