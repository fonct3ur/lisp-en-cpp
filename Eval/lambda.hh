#pragma once

#include <iostream>
#include <stdexcept>
#include <assert.h>
#include <string>
#include "globals.hh"
#include "library.hh"
#include "defs.hh"
#include "eval.hh"


bool lambdalistp(Object obj);
Object apply_lambda(Object fun, Object args, Env env);
Env add_to_env(Object vars, Object args, Env env);
