#include <cstdio>

#include "toplevel.hh"

using namespace std;

int main()
{
	Toplevel toplevel;

	try
	{
		toplevel.go();
	}
	catch (runtime_error& e)
	{
		clog << "May Lisp be with you" << endl;
	}
	return 0;
}
