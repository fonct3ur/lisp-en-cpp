#include <iostream>
#include <stdexcept>
#include <assert.h>
#include <string>
#include <iostream>
#include <sstream>
#include "globals.hh"
#include "API.hh"
#include "library.hh"
#include "defs.hh"

using namespace std;


// Basic functions mirroring Lisp functions

void check(Object l) { return API::check(l); }

Object nil() { return API::nil(); }

bool null(Object l) { return API::null(l); }

Object t() { return API::t(); }

Object f() { return API::f(); }

Object get_func_lambda()
{
	return API::get_func_lambda();
}

Object cons(Object a, Object l)
{
	return API::cons(a,l);
}

Object car(Object l)
{
	try
	{
		return API::car(l);
	}
	catch(string e)
	{
		throw string ("error in a car.");
	}
}

Object cdr(Object l)
{
	try
	{
		return API::cdr(l);
	}
	catch(string e)
	{
		throw string ("error in a cdr.");
	}
}

int get_size_list(Object l)
{
	return API::get_size_list(l);
}

bool eq(Object a, Object b) { return API::eq(a,b); }


// Test functions

bool is_boolean(Object a) { return eq(a, t()) || eq(a, f()); }
bool is_true(Object a) { return API::object_to_bool(a);}
bool is_false(Object a) { return not(is_true(a)); }

bool numberp(Object l) { return API::numberp(l); }
bool stringp(Object l) { return API::stringp(l); }
bool symbolp(Object l) { return API::symbolp(l); }
bool listp(Object l) { return API::listp(l); }
bool funcp(Object l) { return API::funcp(l); }
bool constantp(Object l) { return API::constantp(l); }


bool lazy_funcp(Object l) { return API::lazy_funcp(l); }
bool eager_funcp(Object l) { return API::eager_funcp(l); }
bool identify_func(std::string s, Object l) { return API::identify_func(s, l); }

Object number_to_object(int n) { return API::number_to_object(n); }
Object string_to_object(std::string s) { return API::string_to_object(s); }
Object symbol_to_object(std::string s) { return API::symbol_to_object(s); }
Object bool_to_object(bool b) { return API::bool_to_object(b); }


// Conversion functions

int object_to_number(Object l)
{
	try
	{
		return API::object_to_number(l);
	}
	catch(string e)
	{
		throw string ("not a number : " + object_to_string(l) + ".");
	}
}

bool object_to_bool(Object l)
{
	return API::object_to_bool(l);
}

std::string object_to_string(Object l)
{
	return API::object_to_string(l);
}


// Print functions

std::ostream& object_to_stream(std::ostream& s, Object l)
{
	return API::object_to_stream(s, l);
}

std::ostream& operator<<(std::ostream& s, Object l)
{
	return object_to_stream(s ,l);
}

int get_nb_objects_initialized()
{
	return API::get_nb_objects_initialized();
}


// Derived functions

Object cadr(Object l) { return car(cdr(l)); }
Object cddr(Object l) { return cdr(cdr(l)); }
Object caddr(Object l) { return car(cdr(cdr(l))); }
Object cdddr(Object l) { return cdr(cdr(cdr(l))); }
Object cadddr(Object l) { return car(cdr(cdr(cdr(l)))); }



// Environment handling functions
Env make_env()
{
	return API::make_env();
}

Env add_new_binding(std::string name, Object value, Env env)
{
	return API::add_new_binding(name,value,env);
}

Object find_value(std::string name, Env env)
{
	return API::find_value(name, env);
}

bool in_envp(std::string name, Env env)
{
	return API::in_envp(name, env);
}

std::ostream& print_env(std::ostream& s, Env env)
{
	return API::print_env(s, env);
}
