#pragma once

#include <iostream>
#include <stdexcept>
#include <string>
#include "globals.hh"

// Object

void check(Object l);

Object nil();
bool null(Object l);
Object t();
Object f();
Object get_func_lambda();
Object cons(Object a, Object l);
Object car(Object l);
Object cdr(Object l);
Object cdr(Object l);
bool eq(Object a, Object b);
int get_size_list(Object l);

bool is_true(Object a);
bool is_false(Object a);
bool numberp(Object l);
bool stringp(Object l);
bool symbolp(Object l);
bool listp(Object l);
bool funcp(Object l);
bool constantp(Object l);
bool lazy_funcp(Object l);
bool eager_funcp(Object l);
bool identify_func(std::string s, Object l);

Object number_to_object(int n);
Object string_to_object(std::string s);
Object symbol_to_object(std::string s);
Object bool_to_object(bool b);
int object_to_number(Object l);
bool object_to_bool(Object l);
std::string object_to_string(Object l);

std::ostream& object_to_stream(std::ostream& s, Object l);
std::ostream& operator<<(std::ostream& s, Object l);

int get_nb_objects_initialized();

// Derived function

Object cadr(Object l);
Object cddr(Object l);
Object caddr(Object l);
Object cdddr(Object l);
Object cadddr(Object l);


// Env
Env make_env();
Env add_new_binding(std::string name, Object value, Env env);
Object find_value(std::string name, Env env);
bool in_envp(std::string name, Env env);
std::ostream& print_env(std::ostream& s, Env env);
